// códigos que existem em todas as páginas separados em arquivos.

$("#inc-header").load("inc/inc-header.html"); 
$("#inc-newsletter").load("inc/inc-newsletter.html"); 
$("#inc-footer").load("inc/inc-footer.html"); 
$("#inc-fixo").load("inc/inc-fixo.html");
$("#inc-aside").load("inc/inc-aside.html");

// active menu
$(document).ready(function () {
    console.log(window.location.href);
    $('a[href]').each(function () {
        if (window.location.href.indexOf($(this).attr('href')) > -1) {
            $(this).addClass('active');
        }
    });
});
