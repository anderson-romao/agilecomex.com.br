// HOME -> especialistas
$(document).ready(function(){
	$('.carousel-especialistas').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		variableWidth: true,
		centerMode: true,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left-wine.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right-wine.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
				centerMode: false,
				variableWidth: false,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				centerMode: false,
				variableWidth: false,
			}
		}
		]
	});
});

// HOME -> depoimento
$(document).ready(function(){
	$('.carousel-depoimento').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left-wine.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right-wine.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 1,
				variableWidth: false,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				variableWidth: false,
			}
		}
		]
	});
});

// SOLUÇÕES INTERNO -> outros segmentos
$(document).ready(function(){
	$('.carousel-segmentos').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left-red.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right-red.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 1,
				variableWidth: false,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				variableWidth: false,
			}
		}
		]
	});
});


// SOBRE -> historia
$(document).ready(function(){
	$('.carousel-fotos-descricao').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		fade: true,
		asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
		slidesToShow: 6,
	  	slidesToScroll: 1,
	  	asNavFor: '.carousel-fotos-descricao',
	  	dots: false,
	  	arrows: false,
	  	centerMode: false,
	  	focusOnSelect: true,
	  	responsive: [
	  	{
	  		breakpoint: 1025,
	  		settings: {
	  			slidesToShow: 4,
	  		}
	  	}, {
	  		breakpoint: 769,
	  		settings: {
	  			slidesToShow: 3,
	  		}
	  	}, {
	  		breakpoint: 520,
	  		settings: {
	  			slidesToShow: 2,
	  		}
	  	}
	  	]
	});
});

//Iniciar animações
AOS.init({
	disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});